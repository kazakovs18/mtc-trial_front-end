// Vendor resources (normalize.css, jquery and plugins by your choice)
require("./vendor/normalize.min.css");
window.$ = window.jQuery = require("./vendor/jquery-3.3.1.min");

// Your own CSS files
require("./vendor/lightslider.min.css");
require("./scss/style.scss");

// Your own javascript files
require("./js/app.js");
require("./vendor/lightslider.min.js");